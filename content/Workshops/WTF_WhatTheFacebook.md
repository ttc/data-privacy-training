# What the Facebook?
--- meta
title: What the Facebook?
uuid: 36910960-b189-4a62-8943-f9787be621be
lan: en
source: Tactical Tech
item: SLE
tags:
  - Social media
  - Debate
  - Quiz
duration: 180
description: "Facebook knows you intimately, but how well do you know Facebook? Look past Facebook the platform to Facebook the company, and see what their business model means for you and your personal information. Then find out how you can use Facebook without sharing quite so much."
---

## Meta information

### Description
Facebook knows you intimately, but how well do you know Facebook? Look past Facebook the platform to Facebook the company, and see what their business model means for you and your personal information. Then find out how you can use Facebook without sharing quite so much.

### Overview
1. Introductions (10 min)
2. You and Facebook - Part 1 (10 min)
3. You and Facebook - Part 2 (10 min)
4. Facebook's Products and Services (20 min)
5. Data Collection and Profiling (30 min)
6. Facebook's Data Policy (30 min)
7. Strategies (40 min)
8. Facebook's Privacy Settings (15 min)
9. Wrap up (15 min)

### Duration
180 minutes (excluding breaks)

### Ideal Number of Participants
Minimum of 3.

### Learning Objectives
##### Knowledge
- Understand Facebook as a multifaceted company (know which services Facebook owns, and how these are interconnected).
- Learn how Facebook collects data from its users, what kinds of data they collect, and what the company uses it for.

##### Skills
- Be better able to manage personal data, identity and social networks.

##### Attitude
- Privacy has a lot of grey areas. To navigate our way through these, we should ask: Who has access to our data and who do we want to manage our identity and social network? What information can be public, what information do we want to share with trusted individuals, and what information do we want to keep to ourselves?

### References

#### Background Reading
- [Facebook app poses a dilemma for publishers](http://www.theguardian.com/media/2014/nov/09/facebook-app-dilemma-for-publishers-content-social-networking)(The Guardian)
- [Why the Facebook Messenger app is not the privacy nightmare people think it is](https://nakedsecurity.sophos.com/2014/08/12/why-the-facebook-messenger-app-is-not-the-privacy-nightmare-people-think-it-is/) ("Naked Security," Sophos)
- [5 tips to make your Facebook account safer](https://nakedsecurity.sophos.com/2014/05/27/5-tips-to-make-your-facebook-account-safer-updated/) ("Naked Security," Sophos)
- [How to Quit Facebook without actually quitting Facebook](http://lifehacker.com/5538697/how-to-quit-facebook-without-actually-quitting-facebook)  (Lifehacker)
- [Top ten reasons you should quit Facebook](http://gizmodo.com/5530178/top-ten-reasons-you-should-quit-facebook)  (Gizmodo)
- [List of mergers and acquisitions by Facebook](https://en.wikipedia.org/wiki/List_of_mergers_and_acquisitions_by_Facebook) (Wikipedia)
- ["Custom Facebook Tools"](https://inteltechniques.com/intel/osint/facebook.html)  (Intel Techniques)
- [Facebook graph search engine](http://graph.tips/)
- [Moments app](http://www.momentsapp.com/)

#### Useful resources for the session
- [Facebook how-to](https://securityinabox.org/en/guide/social-networking/internet) (Security in a box, Tactical Tech)
- [Lost in Small Print - Facebook's Privacy Policy](https://myshadow.org/lost-in-small-print/facebooks-data-policy)
- Trailer: [Terms and Conditions May Apply](http://tacma.net/tacma.php) (Dir: Cullen Hoback)
- [Blue Feed, Red Feed](http://graphics.wsj.com/blue-feed-red-feed/)  (WSJ)
- [Immersion](https://immersion.media.mit.edu/)  (MIT)
- What Facebook Thinks You Like tool (find it in the Chrome web store)

#### Useful Case Studies
1. Care insurer Admiral worked on an app that would use your Facebook posts to link your personality traits to your driving style. Do you use "!!!!" and the words "Always", "Never" and "Maybe"? According to Admiral, you have an overconfident personality and under this system would not be eligible for a safe driving discount. In the end, Admiral was forced by Facebook to scrap the plan just hours before launch, as it was deemed to break Facebook's privacy rules.
https://ttc.io/ZZ9

2. "Facebook Lets Advertisers Exclude Users by Race?" Based on your hometown and language, Facebook gives you an 'Ethnic Affiliation' tag - which ProPublica found can be used to exclude entire groups from advertising campaigns.
https://ttc.io/ZZC
\\\[update:\\\]
http://www.nytimes.com/2016/11/12/business/media/facebook-will-stop-some-ads-from-targeting-users-by-race.html

3. Why One Mom Tried To Hide Her Pregnancy From The Internet (Huffington Post)
“If you’re a pregnant woman, it’s usually impossible to make it through your pregnancy without a single diapers ad.” Trying to avoid ad targeting on Facebook is hard! If you're pregnant, this is valuable information for marketers: your spending patterns and interests are about to change. Read this story about Princeton sociology professor Janet Vertesi, who decided to try and hide her pregnancy from Facebook.
https://ttc.io/ZZF

### Materials and Equipment Needed
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](67c7149b-44f9-4b7a-b4ee-c8bf786b50dc)
- @[material](c0358b51-fe16-47ae-9686-927ec39d18f6)
- @[material](10492a69-87ad-49c7-8cd8-764815ab5da4)
- @[material](9793b1a6-5b29-4831-b7b7-05fcc5d31891)


## Steps

### Step 1: Introductions (10 min)
1. Briefly introduce yourself and the session, then ask participants to introduce themselves and answer the following questions:
    - Do you use Facebook or have you ever used Facebook, and what do/did you use it for?
    - What do you want to learn in this session?
2. Taking expectations into account, give a brief overview, including objectives, what will be covered (and what not), and how much time is available.

### Step 2: You and Facebook - Part 1 (10 min)
For the following activity, use statements like:
1. Everyone who has logged into Facebook in the last 24 hours (this one can be interesting because some people are just always logged in)
2. Everyone who has ever tagged themselves or someone else in a photo on Facebook.

##### Activity: "Line Up According to...."
@[activity](9603b159-bf52-428b-a8a0-902486d2205e)

### Step 3: You and Facebook - Part 2 (10 min)
Use the Facebook Exposure Quiz to get participants thinking about their exposure on Facebook. Adapt if necessary.


##### Activity: "Exposure Quiz"
For this activity, use the Facebook Exposure Quiz.

@[activity](b93d26d9-82b1-46bd-8403-4bf73ae6fb25)

### Step 4: Facebook's Products and Services (20 min)
The following activity aims to get participants thinking about Facebook as a company, and to introduce them to how big a company it actually is.

##### Activity: "Word Search"
@[activity](20b6a14f-cb4b-4856-941f-c9af273636f1)


### Step 5: Data Collection and Profiling (30 min)
This activity follows on from the Word Search Activity. Use the list if data traces to lead a discussion: What insights might Facebook get into your life, habits and social networks?

In the context of Facebook, the following are worth bringing particular attention to:
  - Moments: pictures of your face, face recognition, who your friends are
  - Newsfeed: who you are closest to, what you are interested in
  - Friends: your social graph  (this can be illustrated through the Immersion demo)
  - data sharing: how much data is shared between the different services, e.g. Facebook, Moments, Instagram, Whatsapp? This can be done as an actual investigation exercise, or just touched upon as something to think about.

##### Activity: Word Search - "Data Traces" (20 min)
@[activity](afa7b0e5-e772-4523-a01b-381fb5d16e49)


### Step 6: Facebook's Data Policy (30 min)
What does Facebook use your data for? The following activity should be run with a focus on Facebook's data policy.

The following activity should be run with a focus on Facebook's privacy policy.
@[activity](f690c52d-b314-4f05-bead-47a5dc141c78)


### Step 7: Strategies (40 min)
1. Split participants into 3-4 groups and give them 15-20 minutes to come up with strategies for better managing their data and privacy on Facebook, with reference to the services and data traces found in the Word Search done earlier.
2. Bring participants back together to compare and pool strategies.
    - Examples:
      - Photos -  make sure that you don't tag people;
      - Location, ensure that geolocation services are off
      - Identity management
      - etc.

### Step 8: Facebook's Privacy Settings (15 min)
1. Use this last session to go through Facebook's privacy settings, using a projector connected to a laptop. Use the Facebook How-to on Security in-a-box (https://securityinabox.org)
2. You can make the session more interactive by getting participants to explain the settings themselves.

### Step 9: Wrap up (15 min)
Ask participants if there questions, concepts that are unclear or general observations, and provide them with external resources.

-------------------------------
<!---
data-privacy-training/Exhibition/TEMPLATE
-->
