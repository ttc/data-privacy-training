# Mobile Communication
--- meta
title: Mobile Communication
uuid: be6287dd-2a2f-4679-8cd1-6771c481e3ea
lan: en
source: Tactical Tech
item: SLE
tags:
  - Mobile
  - Drawing exercise
duration: 150
description: How does mobile communication work? Who has access to your information along the way? And what you can do to take back some control over what information others have access to?
---

## Meta information

### Workshop Description
How does mobile communication work? Who has access to your information along the way? And what you can do to take back some control over what information others have access to?

### Overview
1. Introductions (10 min)
2. How mobile communication works (45 min)
3. Your phone: A breakdown in four layers (45 min)
4. What can you do? Tips on managing your traces (20 min)
5. Consolidation quiz (10 min)
6. Wrap up: Questions, clarifications and resources (10 min)

### Duration
150 minutes (excluding breaks)

### Ideal Number of Participants
15-20.

### Learning Objectives
##### Knowledge
- Understand the basics of mobile technology: how a mobile phone and mobile communications infrastructure work, and what data traces you leave behind.
- Understand the trade-offs and basic security issues related to Android, iPhone, Windows and Feature phones respectively.

### References
- [Mobile Tools, Security in-a-box](https://securityinabox.org/en/mobile-tools)(Tactical Tech)
- [Mobile Safety](https://www.level-up.cc/curriculum/mobile-safety/) (Level-Up Trainers' Curriculum)
- [Umbrella, app for Android](https://play.google.com/store/apps/details?id=org.secfirst.umbrella&hl=en) (Security First)


### Materials and Equipment Needed
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](a1108a3a-7bd8-4afc-9255-02a181ccffef)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)
- @[material](c0358b51-fe16-47ae-9686-927ec39d18f6)
- @[material](16c01d17-9ba7-47d6-815a-75cf9633004f)
- @[material](b6be8eed-7382-4594-bbe1-eaf471f8f081)
- @[material](412c1eb9-9fc6-4f6b-975a-6d7c1915f750)
- @[material](1d01a081-f4a9-408b-b06b-78ee6288d1ce)
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](9c584197-c4e1-4128-b2cf-60f91e47d52d)

### Optional Materials and Handouts
- @[material](9824a3dd-12ed-4684-9afa-dd75118404ee)



## Steps

### Step 1: Introductions (10 min)
1. Briefly introduce yourself and the session, then ask participants to introduce themselves and answer the following questions:
    - What do you use your phone for?
    - What do you want to learn in this session?

2. Taking expectations into account, give a brief overview of the session, including objectives, what will be covered (and what not), and how much time is available.

### Step 2: The basics of mobile communication (45 min)
The following activity allows participants to see how mobile communication actually works, and what information third parties have access to along the way.


##### Activity: "How Mobile Communication Works"
@[activity](541f7a7b-e83f-4018-8646-b86fcbccca4)


### Step 3: How does a phone work? What third parties have access, to which parts? (40 min)
The following activity looks at how a mobile phone works, and how different parts of the mobile phone are used for information collection and tracking.


##### Activity: "Your Mobile Phone - A breakdown in 4 Layers"
@[activity](be34b4fa-dbc9-4b15-aa3e-b9354ddf5ee8)

### Step 4: What can you do? Tips on managing your traces (20 min)
Walk participants through the Hands-On Checklist of the _Mobiles Reference Document_. Since this is not a hands-on session, run through these as a general overview.


### Step 6: Mobile Communication: Consolidation Quiz (10 min)
Download the Mobile Communication Quiz. The quiz can be done using a projector or by reading out the questions, or it can be done individually with printed-out forms.


### Step 06: Wrap up (10 min)
1. See if anything is unclear, and answer questions
2. Direct participants to resources
3. Hand out Tactical Tech's _Pocket Privacy for Mobiles_ if you have them.


-------------------------------
<!---
data-privacy-training/SLEs/TEMPLATE
-->
