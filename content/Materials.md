# Materials

## Projector
--- meta
uuid: ce457811-1423-4ff0-93bb-7bc2fda1e844
tags:
  - material
---


## Computer
--- meta
uuid: 0d1c2469-bc55-41da-8207-63edf8fd307b
tags:
  - material
---


## Participants' computers
--- meta
uuid: e96c589f-f1c5-49de-8493-ca39de05a502
tags:
  - material
---


## Participants' phones
--- meta
uuid: 6d758ada-e6cf-4a56-a96b-f84dfe14181c
tags:
  - material
---


## Internet connection
--- meta
uuid: f6cd74bf-5a89-4fdc-8122-e305f947e14c
tags:
  - material
---


## A4 Paper
--- meta
uuid: 16c01d17-9ba7-47d6-815a-75cf9633004f
tags:
  - material
---


## Pens
--- meta
uuid: b6be8eed-7382-4594-bbe1-eaf471f8f081
tags:
  - material
---


## Markers
--- meta
uuid: 9392dacf-999c-4c33-a6d8-4545c1aee849
tags:
  - material
---


## Post-its
--- meta
uuid: c0358b51-fe16-47ae-9686-927ec39d18f6
tags:
  - material
---


## Flip chart
--- meta
uuid: 417fc8a5-400f-4553-a23e-faa949beb239
tags:
  - material
---


## General workshop materials
--- meta
uuid: 67c7149b-44f9-4b7a-b4ee-c8bf786b50dc
tags:
  - material
---


## Envelopes
--- meta
uuid: 2c06216d-1383-4ec0-9048-5c5cc51cdde0
tags:
  - material
---


## Polariod camera
--- meta
uuid: 388e928a-47b8-4cb0-8e43-dc7ec2fc8c86
tags:
  - material
---


## Mobile phone breakdown in 4 layers
--- meta
uuid: a1108a3a-7bd8-4afc-9255-02a181ccffef
tags:
  - material
links:
  - label: Mobile Phone breakdown in 4 layers
    link: "https://myshadow.org/ckeditor_assets/attachments/94/mobile_breakdowna5.pdf"
---


## Cards - How the Internet Works
--- meta
uuid: 1d01a081-f4a9-408b-b06b-78ee6288d1ce
tags:
  - material
links:
  - label: How the Internet Works - printable cards
    link: "https://myshadow.org/ckeditor_assets/attachments/146/howtheinternetworks_cardset.pdf"
---


## Cards - How Mobile Communication Works
--- meta
uuid: 9c584197-c4e1-4128-b2cf-60f91e47d52d
tags:
  - material
links:
    - label: How Mobile Communication Works - printable cards
      link: "https://myshadow.org/ckeditor_assets/attachments/161/howmobilecommsworks_cardset.pdf"
---


## Choosing tools - blank grid
--- meta
uuid: 6ebfc3d0-3f8a-41b9-800b-5e802b985fd8
tags:
  - material
links:
  - label: Choosing Tools - blank grid
    link: "https://myshadow.org/ckeditor_assets/attachments/126/choosingtools_emptyframework.pdf"
---


## Choosing Tools - Chat Apps
--- meta
uuid: 5cf30710-29f7-42e9-adef-eb935a54a981
tags:
  - material
links:
  - label: Choosing Tools - Chat Apps
    link: "https://myshadow.org/ckeditor_assets/attachments/170/choosing_chatapps.pdf"
---


## Communication Model
--- meta
uuid: b54e63a1-5ea6-4337-bb16-efc4fcc7b95c
tags:
  - material
---


## Location tracking
--- meta
uuid: ec71a937-d4df-472c-9a95-a388f70940dd
tags:
  - material
---


##  Facebook Wordsearch & Answer Sheet
--- meta
uuid: 9793b1a6-5b29-4831-b7b7-05fcc5d31891
tags:
  - material
links:
  - label: Facebook Wordsearch
    link: "https://myshadow.org/ckeditor_assets/attachments/119/facebook_wordsearch.pdf"
  - label: Facebook Wordsearch - Answer Sheet
    link: "https://myshadow.org/ckeditor_assets/attachments/153/facebookproducts_services_v1_0.pdf"
---


## Google Wordsearch & Answer Sheet
--- meta
uuid: f2afe09c-14df-4e54-9e45-ed8062e4ef78
tags:
  - material
links:
  - label: Google Wordsearch
    link: "https://myshadow.org/ckeditor_assets/attachments/121/google_wordsearch.pdf"
  - label: Google Wordsearch - Answer Sheet
    link: "https://myshadow.org/ckeditor_assets/attachments/118/googleproductsservices.pdf"
---


## Comic Templates
--- meta
uuid: e664e54e-f6cb-4e88-bc1e-cbdfed060456
tags:
  - material
links:
  - label: Comic Template 01
    link: "https://myshadow.org/ckeditor_assets/attachments/140/blank_comic_page1.pdf"
  - label: Comic Template 02
    link: "https://myshadow.org/ckeditor_assets/attachments/141/blank_comic_page2.pdf"
---


## Browser Quiz
--- meta
uuid: c1b99fb2-eeda-4c25-8c7a-3e882a828c5e
tags:
  - material
links:
  - label: Browser Quiz  
    link: "https://myshadow.org/ckeditor_assets/attachments/162/browserquiz_v1_0.pdf"
---


## Mobile Communication Quiz
--- meta
uuid: ea903d2a-586b-4915-a921-b5aa17da0d9b
tags:
  - material
links:
  - label: Mobile Communication Quiz
    link: "https://myshadow.org/ckeditor_assets/attachments/163/mobilecommunicationquiz_v1_0.pdf"
---


## Exposure Quiz: Facebook
--- meta
uuid: 10492a69-87ad-49c7-8cd8-764815ab5da4
tags:
  - material
links:
  - label: Exposure Quiz - Facebook
    link: "https://myshadow.org/ckeditor_assets/attachments/152/exposurequiz_v1_0.pdf"
---


## Pocket Privacy Guide - Browser
--- meta
uuid: ce39f7f2-0357-419a-aca7-81806e00e6cf
tags:
  - material
links:
  - label: Pocket Privacy Guide Browser - low-res
    link: "https://myshadow.org/ckeditor_assets/attachments/93/pp_browser.pdf"
---


## Pocket Privacy Guide - Mobile
--- meta
uuid: 9824a3dd-12ed-4684-9afa-dd75118404ee
tags:
  - material
links:
  - label: Pocket Privacy Guide Mobile - low-res
    link: "https://myshadow.org/ckeditor_assets/attachments/134/mobiles.pdf"
---


## Mobiles Reference Document
--- meta
uuid: 412c1eb9-9fc6-4f6b-975a-6d7c1915f750
tags:
    - material
links:
  - label: Mobiles Reference Document
    link: "https://myshadow.org/ckeditor_assets/attachments/159/mobiles_reference_document_v1_0.pdf"
---


## Browser Reference Document
--- meta
uuid: 1544909d-e400-4588-a4cb-b638a204422e
tags:
    - material
links:
  - label: Browser Reference Document
    link: "https://myshadow.org/ckeditor_assets/attachments/158/browser_reference_documentv1_0.pdf"
---


## Strategies of Resistance framework
--- meta
uuid: 884b8427-f8ba-441c-a663-b32282aaeb41
tags:
    - material
links:
  - label: Strategies of Resistance - a framework
    link: "https://myshadow.org/ckeditor_assets/attachments/157/strategies_of_resistance_v1_0.pdf"
---


## Introduction to Data
--- meta
uuid: 5cd65e76-c2fd-4586-b889-abb5dd6cd836
tags:
    - material
links:
  - label: Introduction to Data
    link: "https://myshadow.org/ckeditor_assets/attachments/160/introtodata_referencedocumentv1_0.pdf"
---
