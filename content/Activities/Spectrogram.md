# Spectogram
--- meta
title: Spectogram
uuid: 15272a79-dff5-4f7c-8faf-5ae4f1e35a05
lan: en
source: Tactical Tech, adapted from an exercise (_Spectogram_) written up by Daniel O'Clunaigh, Lindsay Beck, and DJ, for [Level Up](https://www.level-up.cc/activity/spectrogram)
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Collecting ideas
  - Debate
duration: 30
description: Get a sense of what people in the room think about a specific issue, and kick-start discussion.
---

## Meta information

### Description
Get a sense of what people in the room think about a specific issue, and kick-start discussion.


### Duration
40 minutes.


### Ideal Number of Participants
Minimum of 5 for an active discussion.


### Learning Objectives
##### Knowledge
- Get a sense of where people stand on specific issues.

##### Skills
- Practice arguing specific topics.


### References
- [Spectogram](https://www.level-up.cc/activity/spectrogram) (Level-up)


### Materials and Equipment Needed
- Open space big enough for a group to spread out.  


## Activity: "Spectogram"
--- meta
uuid: 57c2c69d-b55c-4fa7-addc-91fd5431ad53
---

#### Preparation
This exercise involves debating statements. You can either crowd-source statements from the group, or introduce pre-prepared statements. When preparing statements, think of ones that are likely to divide the room in terms of opinion, and thereby make for lively discussion.

#### Demo (10 min)
Ask for three volunteers to help demonstrate the activity, and then:
1. Designate one side of the room as "Strongly Agree", and the other side "Strongly Disagree."
2. Introduce a statement. For this demo, use something banal: "Cats are better than dogs," or "The weather here is great." Have the volunteers move to demonstrate the views "strongly agree (one side)," "strongly disagree(the other side)," and "don't know (in the middle)."

#### Write down statements (10 min)
1. Break participants into smaller groups and tell them they're going to do a spectogram related to X (whatever the topic is).
2. Each person in the group should write on a post-it (one statement per post-it) at least one strong statement related to this topic.
3. Ask each group to choose one statement, and then bring everyone back together and collect the statements.

#### Debate using the spectogram (20 min)
1. Ask a volunteer to read out the first statement. Each person should choose a position on the spectogram.
2. Ask someone (preferably on the Strongly Agree or Strongly Disagree side) to explain why they agree or do not agree. After they have given their opinion, allow everyone to shift their position if their views have changed. Ask a few more people to explain their positions.
3. Move on to another statement.

### Variations
1. As noted above, this activity can be done with either pre-prepared or crowd-sourced statements.
2. You can turn this into a role-playing exercise by giving participants specific roles e.g. Politician, Law Enforcement, Privacy Activist, Facebook executive, etc. They will then stand on the spectogram as their character.
