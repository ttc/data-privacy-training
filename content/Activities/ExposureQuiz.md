# Exposure Quiz
--- meta
title: Exposure Quiz
uuid: c6026e85-ec21-47d0-a5c8-dcc57439820c
lan: en
source: Tactical Tech, adapated from an exercise (_What are You Exposing?_) by Daniel O'Clunaigh, Lindsay Beck and DJ for [Level-Up](https://level-up.cc)
item: Activity
tags:
  - Mobile
  - Social media
  - Browser
  - Choosing tools
  - Quiz
duration: 30
description: Quiz exercise to help people assess how exposed they are on a particular service.
---

## Meta information


### Description
Quiz exercise to help people assess how exposed they are on a particular service (e.g. Facebook)

### Duration
20-30 minutes.


### Ideal Number of Participants
Works with any number of participants.


### Learning Objectives
##### Knowledge
- Understand how exposed you are when using a specific service.


### Materials and Equipment Needed
- Example Exposure Quiz: @[material](10492a69-87ad-49c7-8cd8-764815ab5da4)


## Activity: "Exposure Quiz"
--- meta
uuid: b93d26d9-82b1-46bd-8403-4bf73ae6fb25
---

#### Preparation
1. Create a quiz for the platform you want to focus on, or see if one already exists.
2. Print out one Exposure Quiz for each participant.


#### Do the quiz (15 min)
1. Hand out the quiz and note the time available to fill it out.
2. Note for participants: where more than one option applies, the option with the highest number should be chosen.

#### Count scores and have a follow-up discussion (15 min)
1. After participants have finished they should add together the points of their answers (e.g. 'answer 1 = 1 point, answer 2 = 2 points etc) to calculate their exposure.
2. Use the quiz as a basis for a short discussion with the group as a whole.

-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
