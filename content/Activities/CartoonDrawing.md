# Cartoon Drawing
--- meta
title: Cartoon Drawing
uuid: 393126b3-3ee3-4d03-899a-c4926abd30c7
lan: en
source: Tactical Tech. Adapted from an exercise presented by John Fass - Royal College of Art
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Drawing exercise
duration: 20
description: Make abstract digital concepts personal and tangible.
---

## Meta information

### Description
Make abstract digital concepts personal and tangible.

### Duration
20-30 minutes.

### Ideal Number of Participants
Minimum of 4.

### Learning Objectives
##### Knowledge
- Gain a more concrete understanding of a process, system or pattern of interaction.


### Materials and Equipment Needed
- @[material](b6be8eed-7382-4594-bbe1-eaf471f8f081)
- @[material](e664e54e-f6cb-4e88-bc1e-cbdfed060456)


## Activity: "Cartoon Drawing"
--- meta
uuid: ccc6145d-1ddf-4431-96fc-f94987213bc1
---

#### Preparation
Print out comic templates - at least one per participant.

#### Drawing exercise  (20-30 min)
1. **Draw**: Hand out the comic templates and pens, and ask participants to draw a specific process (for example, 'what you use the internet for on an average day').
2. **Compare**: Divide participants into small groups to discuss differences and similarities in each others' drawings, and to identify anything specific that stands out.
3. **Feedback session**: Participants come back together to report back on what they found.


-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
