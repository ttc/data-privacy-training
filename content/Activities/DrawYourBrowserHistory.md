# Draw Your Browser History
--- meta
title: Draw Your Browser History
uuid: ad052ce9-29c9-4f76-b94e-0c203a2079f0
lan: en
source: Tactical Tech
item: Activity
tags:
  - Drawing exercise
  - Browser
  - Mobile
duration: 45
description: Get a better sense of how you use your browser, and what types of data traces are created in the process.
---

## Meta information

### Description
Get a better sense of how you use your browser, and what types of data traces are created in the process.

### Duration
45 - 65 minutes.


### Ideal Number of Participants
This activity can be done with any number of participants.


### Learning Objectives
##### Knowledge
- Understand how you create digital traces through your browser.
- Know what is meant by the terms 'content' and 'metadata', and how they are different.

### References
- Download the _Browser Reference Document_ and the _Introduction to Data_ from the links below, or find them on the [Materials](https://myshaodw.org/materials) page of MyShadow.org.
- For optional last activity, see Bruce Schneier's ['levels of control' framework](https://myshadow.org/how-much-control-do-we-have-over-our-data).

### Materials and Equipment Needed
- @[material](e664e54e-f6cb-4e88-bc1e-cbdfed060456)
- @[material](b6be8eed-7382-4594-bbe1-eaf471f8f081)
- @[material](1544909d-e400-4588-a4cb-b638a204422e)
- @[material](5cd65e76-c2fd-4586-b889-abb5dd6cd836)

### Optional Materials and Handouts
- @[material](ce39f7f2-0357-419a-aca7-81806e00e6cf)


## Activity: "Draw Your Browser History"
--- meta
uuid: 7091510e-7d4f-4de6-9273-7f2d7c12a17a
---

#### Preparation
1. Print out some _Comic templates_ - at least one per person. The Templates come in three options; try and print out at least two of these variations, and then let participants choose.
2. Have the templates and coloured pens available on a table.

#### Draw your browser history (15 min)
1. Participants can fetch templates, pens, and paper from the materials table.  
2. Ask participants to draw their browser history of the day or days before.

#### Compare drawings (15 min)
1. Get participants into small groups of 2-4 to compare drawings: What are similarities, differences or things that stand out?
2. Bring the group back together and have the small groups report back.

#### Discussion: Digital traces (15 min)
Lead the group through a discussion on data traces focusing on content and metadata.
1. What is meant by the terms 'content and metadata'? How are they different?
2. Make two column on the board: Content / Metadata.
3. Which types of digital traces are left through the browser? Put these up on the board in the two columns.

#### Optional - How much control do we have over our data? (15 min)
Introduce [Bruce Schneier's 'levels of control' framework](https://myshadow.org/how-much-control-do-we-have-over-our-data).



-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
