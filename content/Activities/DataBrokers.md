# Data Brokers
--- meta
title: Data Brokers
uuid: 6c9c306e-25a8-47ae-98cb-7c15304c178e
lan: en
source: Tactical Tech. Adapted from activity written up on the [Gender and Tech Resources Community Wiki](https://gendersec.tacticaltech.org/wiki/index.php/Privacy_From_data_shadows_to_data_brokers)
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Role-play exercise
duration: 60
description: This is a follow-up to the activity "Data Shadows". Learn how your data traces are used by commercial entities.
dependencies: "@[snippet](1ee3bef4-3f10-4679-a3d6-788eaafa5850)"
---


## Meta information

### Description
This is a follow-up to the activity "Data Shadows". Learn how your data traces are used by commercial entities.


### Duration
60 minutes.


### Ideal Number of Participants
Minimum of 10.


### Learning Objectives
##### Knowledge
- Gain insight into how the the data broker environment works, and how the data traces we leave feed commercial systems.

### References
- [Me and My Shadow](https://myshadow.org) (Tactical Tech)
- ["Confessions of a Data Broker"](https://myshadow.org/resources/confession-of-a-data-broker?locale=en)


### Materials and Equipment Needed
- @[material](67c7149b-44f9-4b7a-b4ee-c8bf786b50dc)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](2c06216d-1383-4ec0-9048-5c5cc51cdde0)


## Activity: "Data brokers"
--- meta
uuid: 22c1f05f-2c04-4cc0-864d-6d930f17e52b
---

#### Preparation
Create two sets of "data traces" cards using small pieces of paper or card. Some examples are given below - these can be adapted to the context of the group.

##### Card Set 1: Data traces you create about yourself
Examples:
  - You download the Spotify playlist “girls just wanna have fun”.
  - You book a flight to Gran Canaria.
  - You Skype with a friend for two hours.
  - You download Snapchat and send some  pictures.
  - You lose your iPhone, and use the “Find my iPhone” app to find it.
  - You use Wi-Fi in an airport and give your email address and personal details in order to get access.
  - You tweet about your upcoming trip to a human rights conference.
  - You log into the university network and start downloading the movie Fargo.
  - Your phone company locates your phone near a crime scene, around the time the crime took place.

##### Card Set 2: Data traces you create about others
Examples:
  - People reply to your invite to Google+ group.
  - On your request your friends share their geo-coordinates of the party they are at.
  - You sign up for an app that needs access to all of your contacts.
  - On Facebook, you tag someone in a picture of a demonstration
  - You forward a campaign email, written by radical left group, to a friend.

#### Collect data traces (10 min)
1. Break participants into small groups, and give each group a large piece of paper (at least A3).  
2. Ask participants to put their 'data shadow' illustrations, from the previous Data Shadows activity, on the big piece of paper.
3. Give each participant an empty envelope and distribute the cards. Each participant should have at least one from each set.
4. Ask participants to put the cards into their envelopes, and stick the envelopes to their drawings.

#### Create profiles (10 min)
1. All groups should now move clockwise so that they are looking at another group's "data shadows."
2. Groups will now take the role of 'data broker'. They should develop a profile of the other group, based on the images and the envelope contents. They will then prepare a 2-minute "elevator pitch" in which they try and sell these profiles.

#### Sell the profiles (3 min per group)
Groups present their elevator pitches.

#### Discussion (10-30 min)
Lead a discussion on data brokering, covering:
1. What is the data business model?
2. Why does 'ownership' of data matter?
3. What is 'profiling'?
4. How can data collected about you be inaccurate?


-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
