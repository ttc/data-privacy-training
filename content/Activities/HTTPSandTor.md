# HTTPS and Tor
--- meta
title: HTTPS and Tor
uuid: 2c53a124-eb4f-4dec-a6dc-e3d475b82622
lan: en
source: Tactical Tech, based on a tool developed by the Electronic Frontier Foundation
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
duration: 30
description: What information can third parties see when you browse the web or send email? How does it make a difference when you use HTTPS or Tor?
---

## Meta information

### Description
What information can third parties see when you browse the web or send email? How does it make a difference when you use HTTPS or Tor?

### Duration
30 minutes.


### Ideal Number of Participants
Minumum of 4 participants.


### Learning Objectives
##### Knowledge
- See what information third parties can see when you browse the internet or use webmail
- Understand the difference between HTTP and HTTPS
- Understand how Tor works.

### References
- EFF diagram: [Tor and HTTPS](https://www.eff.org/pages/tor-and-https)


### Materials and Equipment Needed
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)


## Activity: "Https and Tor"
--- meta
uuid: 9a32d927-f292-4834-939a-320fa96df94d
---

#### Preparation
1. If some participants have computers (one per group), write the URL up for EFF's diagram for HTTPS and Tor: https://www.eff.org/pages/tor-and-https
2. Alternatively, print out the diagram in each 'mode' (HTTP, HTTPS, Tor) - one set for each group.

#### Explore HTTPS and Tor (10 min)
1. Break participants into small groups  
2. Give the groups some time to explore the 'Tor and HTTPS' diagram, and let them discuss their findings.

#### Feedback and discussion (20 min)
Groups report back on their findings. Discussion should cover:
1. Who has access to your data traces?
2. What is the difference between HTTP and HTTPS?
3. What is Tor?
4. How does Tor anonymise your browsing and block online tracking?



-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
