# Tips: How to run a Hands-on Session
--- meta
title: Hands-on sessions
uuid: 844ed57d-c7b6-428c-bd23-1a39983b0a93
lan: en
source: Tactical Tech
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Hands-on
duration:
description: Guide others through practical ways to increase control over their data.
---

## Meta information

### Description
Guide others through practical ways to increase control over their data.

### Ideal Number of Participants
Since this is a hands-on session, there should be a minimum of two facilitators, ideally with a third facilitator to step in where needed.
- 2-20 participants: 2-3 trainers
- 20-28 participants: 4 trainers


### Materials and Equipment Needed
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](6d758ada-e6cf-4a56-a96b-f84dfe14181c)
- @[material](e96c589f-f1c5-49de-8493-ca39de05a502)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)

## Tips: "How to run a Hands-On Session"
--- meta
uuid: ab6c7bf6-330c-428b-99c8-03aad4552ddf
---

#### Preparation
1. Test out all the tools and settings you'll be installing and/or using in the session.
2. Find or create resources to help participants self-learn along the way.

#### Steps
1. Break the group according to size, number of trainers and, if relevant, operating system (e.g. Android or iPhone). Each group should have at least one trainer.
2. Walk each group through the steps involved, in an interactive way, with step-by-step instructions or guidance projected on the wall or printed out.


-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
