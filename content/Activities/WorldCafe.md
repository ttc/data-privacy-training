# World Cafe
--- meta
title: World Cafe
uuid: 05329274-e8da-4ca2-9a97-e533600c8406
lan: en
source:
item: Activity
tags:
  - Mobile
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Collecting ideas
  - Debate
duration: 60
description: Build discussion by moving around the room in groups, adding to each other's points.
---

## Meta information


### Description
Build discussion by moving around the room in groups, adding to each other's points.


### Duration
60 minutes.

### Ideal Number of Participants
Minimum of 12.


### Learning Objectives
##### Knowledge
- Exchange knowledge and insights around specific topics.


### Materials and Equipment Needed
- @[material](67c7149b-44f9-4b7a-b4ee-c8bf786b50dc)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)


## Activity: "World Cafe"
--- meta
uuid: 9095e1a9-6e56-4faa-a241-65717aff1a71
---

#### Preparation
1. Write down a list of open-ended questions (ie not yes/no) that you would like the groups to discuss.
2. Set up "stations". Participants will be split into groups of 3-5, and each group will need one 'station'. This can be a table, or a designated spot in the room where a large sheet of paper can be put on the floor or stuck to the wall.
3. Write each question as a heading on a large sheet of paper, and place each of these sheets at a different station.


#### Set-up and demo (5 min)
1. Each station needs a 'host', who will stay at one station for the duration of the activity. The host's role is to introduce the question and facilitate the corresponding discussion. When the groups rotate, the host is responsible for explaining to the new group what the previous group discussed, so the new group can build on the discussion rather than starting again.
2. Split the rest of the participants evenly into groups of 2-4. Each group should find a station.

#### Discussion (15-20 min per discussion)
1. Each discussion should be given a given amount of time (eg 20 minutes). One person per group should take notes on the large piece of paper.
2. When the time is up, groups should rotate to the next station. The host should fill them in on the previous discussion, and the new discussion should build on this.
3. Depending on time available, rotate the groups through all the stations.

#### Feedback(10 min)
Bring everyone back together and discuss the exercise together.


-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
