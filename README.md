# Data Privacy Curriculum

The Data Privacy Curricula is a training curriculum that offers workshops and activities around data and privacy.

In this repository there are session outlines for individual activities and workshops that look at how to gain more privacy when using mobile phones, social media and browsers (the internet).

These outlines have been designed specifically for trainers, but also others who want to raise awareness on these topics.

The workshops can be run separately, or can be put together for longer, multiple-day trainings.

The original Data Privacy Curriculum was created by Tactical Tech Collective for the [Me and My Shadow project](https://myshadow.org/train), with input and inspiration from a number of partners working in the privacy and digital security field. Sources include [Level-Up](https://www.level-up.cc), the [Gender and Tech Community Wiki](https://gendersec.tacticaltech.org/wiki/index.php/Complete_manual) and Tactical Tech's [Holistic Security Manual](https://tacticaltech.org/holistic).

The Data Privacy Curricula is licensed under Creative Commons Attribution-ShareAlike 3.0 Unported License.


## How to contribute:
- If you want to contribute to the project please send an email to myshadow@tacticaltech.org
- You can also contribute directly to the project by creating a new activity and/or workshop, and/or localize the current content.
- If you want to work on anything in this repository, see if an issue has been created. If not, open a new issue.
- If you want to join a specific part of the project, contact the person who is working on it by creating a ticket and assigning it to that person.
- After assigning a ticket to yourself, branch the project.
- Select or create a folder with your language group.
- Change or add content as needed, using the Template.
- Create a merge request.


## Code of conduct
- For the diverse group of people who will be using using and contributing to this repository, context is important. Be careful not to assume that because something works for you, it will necessarily work for someone else.
- No illegal activity
